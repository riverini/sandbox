#!/bin/bash
echo -e "Preparing Landing Area";
sudo rm -Rf /Users/riverini/SpacePort/*;
echo -e "Landing Process Started...";
ssh riverini@161.35.55.253 "cd /home/riverini/SpacePort/; rm -R /home/riverini/SpacePort/*
mysqldump -u root -p4e7sSN4zU traiigo > traiigo.sql;
sudo tar -cf traiigo_theme.tar /home/master/public/wp-content/themes/traiigo/;
sudo tar -cf traiigo_plugins.tar /home/master/public/wp-content/plugins/;
sudo tar -cf traiigo_uploads.tar /home/master/public/wp-content/uploads/;
exit";

echo -e "Backup Done, Landing Packages on the SpacePort...";
scp riverini@161.35.55.253:"/home/riverini/SpacePort/*" ~/SpacePort;

echo -e "Clearing the Dock...";
cd ~/SpacePort/;
tar -xf traiigo_theme.tar;
tar -xf traiigo_uploads.tar;
tar -xf traiigo_plugins.tar;

echo -e "Negotiating disebarking permits";
rm -Rf /Applications/XAMPP/xamppfiles/htdocs/traiigo/wp-content/themes/traiigo/
rm -Rf /Applications/XAMPP/xamppfiles/htdocs/traiigo/wp-content/plugins/
rm -Rf /Applications/XAMPP/xamppfiles/htdocs/traiigo/wp-content/uploads/

echo -e "Unloading packages to staging area";

theme=$(find /Users/riverini/SpacePort -type d -name "traiigo");
plugins=$(find /Users/riverini/SpacePort -type d -name "plugins");
uploads=$(find /Users/riverini/SpacePort -type d -name "uploads");

cp -Rf "$theme" /Applications/XAMPP/xamppfiles/htdocs/traiigo/wp-content/themes/;
cp -Rf  "$plugins" /Applications/XAMPP/xamppfiles/htdocs/traiigo/wp-content/;
cp -Rf  "$uploads" /Applications/XAMPP/xamppfiles/htdocs/traiigo/wp-content/;

echo -e "Talking to Database";

DBVar=$(find /Users/riverini/SpacePort -type f -name "traiigo.sql");

/Applications/XAMPP/xamppfiles/bin/mysql -u root traiigo < "$DBVar";

echo -e "Cleaning Up";

sudo rm -Rf /Users/riverini/SpacePort/*;

echo -e "All Done";

open http://localhost/traiigo/

exit;